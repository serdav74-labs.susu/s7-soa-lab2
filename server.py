import logging
import socket
import os
from utils import setup_logging
from pony.orm import *
import threading
from time import sleep
import select as os_select

db = Database()

class Message(db.Entity):
    id = PrimaryKey(int, auto=True)
    body = Required(str)

class SocketAdapter:
    def __init__(self, port_number, parent_logger):
        self.logger = parent_logger.getChild('sock')

        self.sock = socket.socket()
        self.sock.bind(('', port_number))

        self.interrupt_pipe_r, self.interrupt_pipe_w = os.pipe()

        self.thread = None
    
    def listen(self, callback):
        self.thread = threading.Thread(
            target=self._listen,
            args=(callback,)
        )
        self.logger.info(f"Creating new thread (name={self.thread.name})...")
        self.thread.start()
    
    def stop(self):
        self.logger.debug(f"Shutting down: closing socket.")
        os.write(self.interrupt_pipe_w, b'!')
        self.logger.debug(f"Socket closed; joining thread...")
        self.thread.join()

    def wait_for_connection(self):
        pipe = self.interrupt_pipe_r

        # https://stackoverflow.com/questions/32734866/
        rfds, _, _ = os_select.select(
            [pipe, self.sock.fileno()],
            [],
            []
        )
        if pipe in rfds:
            return None, None
        else:
            return self.sock.accept()

    def _listen(self, callback):
        try:
            while True:
                self.sock.listen(1)

                connection, address = self.wait_for_connection()
                if connection is None:
                    break

                self.logger.info(f"Connection from {str(address)}. Receiving data...")

                try:
                    while True:
                        data = connection.recv(4096)
                        if len(data) > 0:
                            decoded_data = data.decode()
                            solution = callback(decoded_data)
                            connection.send(solution.encode())
                        else:
                            raise ConnectionClosedError
                except ConnectionClosedError:
                    self.logger.info("No data received. Closing connection.")
                finally:
                    connection.close()
        finally:
            self.sock.close()


class WordSorter:
    def __init__(self):
        self.logger = logging.getLogger('app.sorter')
        self.logger.info("WordSorter initialized.")

        self.sock = SocketAdapter(port_number=14101, parent_logger=self.logger)

    def start(self):
        self.sock.listen(self.solve)
    
    def stop(self):
        self.sock.stop()
    
    def solve(self, data: str) -> str:
        words = data.split(' ')
        result = list(set(words)) # casting to set eliminates all duplicates
        result.sort()

        self.logger.debug(f'Message \"{data}\" was split in {len(result)} word(s).')
        return '\n'.join(result)



class MessageBoard:
    def __init__(self):
        self.logger = logging.getLogger('app.board')
        self.logger.info("MessageBoard initialized.")

        self.sock = SocketAdapter(port_number=14102, parent_logger=self.logger)

    def start(self):
        self.sock.listen(self.solve)
    
    def stop(self):
        self.sock.stop()
    
    def solve(self, data: str) -> str:
        if data == "LIST":
            self.logger.debug("Listing all messages...")
            return '; '.join(self.list_messages()) or 'No messages!'
        else:
            saved = self.save_message(data)
            self.logger.debug("New message added.")
            return f"Message added: \"{saved}\""

    def list_messages(self) -> list:
        with db_session:
            return list(select(m.body for m in Message))

    def save_message(self, message) -> str:
        with db_session:
            entity = Message(
                body=message
            )
            return entity.body


db.bind(
    provider='sqlite',
    filename='db.sqlite',
    create_db=True
)

db.generate_mapping(create_tables=True)

if __name__ == "__main__":
    setup_logging('INFO')
    logger = logging.getLogger('app')
    logger.info(f"Starting up...")
    
    sorter = WordSorter()
    board = MessageBoard()

    sorter.start()
    board.start()

    try:
        while True:
            sleep(0.1)
    except KeyboardInterrupt:
        logger.info("KeyboardInterrupt; shutting down...")
        pass
    finally:
        logger.debug("Waiting for WordSorter to stop...")
        sorter.stop()
        logger.debug("Waiting for MessageBoard to stop...")
        board.stop()

    logger.info("Exiting.")

