import logging
import sys

def setup_logging(level):
    logger = logging.getLogger('app')
    handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter(
        fmt="{asctime:>16} | {name:16} | {levelname:7} | {message}", style="{"
    )

    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger
