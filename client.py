import socket

host = input('Specify a hostname (empty for localhost): ') or 'localhost'
port_number = int(input('Specify a port number (empty for 14101): ') or 14101)

sock = socket.socket()
sock.connect((host, port_number))

try:
    while True:
        query = input('> ')
        sock.send(query.encode())

        data = sock.recv(4096)
        print(f'-- Server response --\n{data.decode()}\n')
except KeyboardInterrupt:
    sock.close()
    print("\n\nConnection closed.")
